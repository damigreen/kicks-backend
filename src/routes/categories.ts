 import express from "express";
import { CategoryBody } from "../types";
import Category from "../models/categories";

const categoriesRouter = express.Router();

categoriesRouter.get("/", async (_req, res) => {
  const categories =  await Category.find({}).populate("products", { name: 1, brand: 1 });
  res.json(categories.map(cat => cat.toJSON()));
});

categoriesRouter.post("/", async (req, res) => {
  const body: CategoryBody = req.body as CategoryBody;
  try {
    const newCategory = new Category ({
      category_name: body.category_name,
      category_description: body.category_description
    });

    const addedItem = await newCategory.save();
    res.json(addedItem.toJSON());

  } catch(exception) {
    console.log(exception);
  }

});

export default categoriesRouter;
