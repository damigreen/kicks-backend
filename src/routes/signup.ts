import express from "express";
import User from "../models/user";
import bcrypt from "bcrypt";
// import { UserBody } from "../types";
// import User1erEntry, CustomRequest } from "../types";

const signupRouter = express.Router();

interface UserBody {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  username: string;
  admin: boolean;
  password: string;
}

signupRouter.get("/", async (_req, res, next) => {
  try {
    const users = await User.find({});
    return res.json(users.map(user => user.toJSON()));
  } catch (exception) {
    next(exception);
  }
});

signupRouter.post("/", async (req, res, next) => {
  try {
    // const body: UserBody = req.body as UserBody;
    const body: UserBody = req.body as UserBody;
    if (!body.password || body.password === undefined) {
      return res.status(400).json({ error: "password missing" });
    } else if (body.password.length < 3) {
      return res.status(400).json({ error: "Password length must be greater than 3" });
    }

    const saltRound = 10;
    const passwordHash = await bcrypt.hash(body.password, saltRound);

    const user = new User({
      firstName: body.firstName,
      lastName: body.lastName,
      email: body.email,
      phoneNumber: body.phoneNumber,
      username: body.username,
      admin: body.admin,
      passwordHash
    });

    const savedUser = await user.save();
    res.json(savedUser);

  } catch (exception) {
    next(exception);
  }
});

export default signupRouter;
 