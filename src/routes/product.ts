import express from "express";
import Product from "../models/product";
import Category from "../models/categories";
// import { ProductBody, DecodedToken, UserBody } from "../types";
// import { ProductBody, DecodedToken, UserBody, CategoryBody } from "../types";
import { ProductBody, DecodedToken, UserBody } from "../types";
import jwt from "jsonwebtoken";
import User from "../models/user";
// import { ProductBody, Requests } from "../types";

const productRouter = express.Router();

productRouter.get("/", async (_req, res, next) => {
  try {
    const products = await Product.find({}).populate("categories", { category_name: 1 });
    res.json(products.map(prod => prod.toJSON()));
  } catch (exception) {
    next(exception);
  }
});

productRouter.post("/", async (req, res, next) => {
  const body: ProductBody = req.body as ProductBody;
  const auth = req.headers.authorization;

  
  if (body === null) {
    res.status(400).send({ error: "content missing "});
  }
  
  try {
    if (!auth) {
      res.status(401).send({ error: "Authorazation not granted"});
    }

    const token = auth ? auth.substr(7) : "";
    const SECRET = "kasgihaoshalsknaksdlnaskdnglkansdknas";
    const decodedToken: DecodedToken = jwt.verify(token, SECRET) as DecodedToken;
    if(!token || !decodedToken.id) {
      return res.status(401).json({ error: "token missing or invalid" });
    }
    
    const newProduct = new Product({
      name: body.name,
      brand: body.brand,
      color: body.color,
      price: body.price,
      quantity: body.quantity,
      size: body.size,
      sizes: body.sizes,
      colors: body.colors,
      image: body.image,
      gender: body.gender,
      category: body.category
    });

    
    const savedProduct = await newProduct.save();
    
    const newCategory = new Category({
      product: savedProduct._id,
      category_name: body.category,
      category_description: ""
    });
    
    await newCategory.save();
    
    res.json(savedProduct.toJSON())

  } catch(e) {
    console.log(e);
    next(e);
  }
});

// Logic to update a product
productRouter.put("/:id", async (req, res, next) => {
  const body: ProductBody = req.body as ProductBody;
  const auth = req.headers.authorization;


  try {
    if (!auth) {
      res.status(401).send({ error: "Authorazation not granted"});
    }

    const token = auth ? auth.substr(7) : "";
    const SECRET = "kasgihaoshalsknaksdlnaskdnglkansdknas";
    const decodedToken: DecodedToken = jwt.verify(token, SECRET) as DecodedToken;
    if(!token || !decodedToken.id) {
      return res.status(401).json({ error: "token missing or invalid" });
    }


    const productObject = {
      name: body.name,
      brand: body.brand,
      color: body.color,
      price: body.price,
      quantity: body.quantity,
      size: body.size,
      sizes: body.sizes,
      colors: body.colors
    };
  
    const userID = decodedToken.id;
    const user: UserBody = await User.findById(userID) as UserBody;

    if (user.admin === false) {
      res.send(401).send({ error: "Must be admin to update product" });
    }
    const savedProduct = await Product.findByIdAndUpdate(req.params.id, productObject, { new: true });
    if (savedProduct !== null) {
      res.json(savedProduct.toJSON());
    }

  } catch (exception) {
    next(exception);
  }
});

productRouter.delete("/:id", async (req, res, next) => {
  const productID = req.params.id;
  const auth = req.headers.authorization;

  
  try {
    if (!auth) {
      res.status(401).send({ error: "Authorazation not granted"});
    }
  
    const token = auth ? auth.substr(7) : "";
    const SECRET = "kasgihaoshalsknaksdlnaskdnglkansdknas";
    const decodedToken: DecodedToken = jwt.verify(token, SECRET) as DecodedToken;
    if(!token || !decodedToken.id) {
      return res.status(401).json({ error: "token missing or invalid" });
    }
    const userID = decodedToken.id;
    const user: UserBody = await User.findById(userID) as UserBody;
  
    if (user.admin === false) {
      res.send(401).send({ error: "Must be admin to update product" });
    }
    
    await Product.findByIdAndRemove(productID);
    res.status(204).end();

  } catch (exception) {
    next(exception);
  }
});

export default productRouter;
