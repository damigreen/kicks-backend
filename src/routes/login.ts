import express from 'express';
import User from "../models/user";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const loginRouter = express.Router();
const SECRET = "kasgihaoshalsknaksdlnaskdnglkansdknas";

loginRouter.post('/', async (req, res, next) => {
  try {
    const body = req.body;
    const user = await User.findOne({ username: body.username });
    console.log(user);
    
    // const passwordCorrect = user === null ? false : await bcrypt.compare(body.password, user.passwordHash);
    if (!user) {
      return res.status(401).json({ error: "invalid username or password"});
    }

    const passwordCorrect = user === null ? false : await bcrypt.compare(body.password, user.passwordHash);
    if (!passwordCorrect) {
      return res.status(401).json({ error: "invalid username or password" });
    }
    console.log(user._id);

    const userForToken = {
      username: user.username,
      id: user._id
    };

    const token = jwt.sign(userForToken, SECRET);
    
    res.status(200).send({ token, username: user.username, firstName: user.firstName, lastName: user.lastName });

  } catch (e) {
    next(e);
  }
});

export default loginRouter;
