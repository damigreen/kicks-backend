import { UserEntry } from "../types";


/* 
return user object from services
{
  firstName: string
  lastName: String
  email: String
  number: number
  username: string
  passeord: string
}

new mongooseSchema()
*/

const getUserEntry = (entry: UserEntry) => {
  return {
    ...entry
  }
}