import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

const categoriesSchema = new mongoose.Schema({
  product:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product"
  },
  category_name: String,
  category_description: String
});
categoriesSchema.plugin(uniqueValidator);

export default mongoose.model("Categories", categoriesSchema);