import mongoose from "mongoose";
import uniqueValidator from 'mongoose-unique-validator';
// import { ProductEntry } from "../types";

const productSchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  brand: { type: String, required: true },
  color: String,
  price: Number,
  quantity: Number,
  gender: String,
  size: Number,
  image: String,
  images: [{ type: String }],
  sizes: { type: [String] },
  colors: { type: [String] },
  category: String,
  categories:[
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Categories"
    }
  ]
});
productSchema.plugin(uniqueValidator);

productSchema.set('toJSON', {
  transform: (_document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  }
});

export default mongoose.model('Product', productSchema);
