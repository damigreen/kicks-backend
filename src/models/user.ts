import mongoose, {Schema, Document } from "mongoose";
import uniqueValidator from 'mongoose-unique-validator';
// import { UserEntry } from "../types";


interface IUser extends Document {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  username: string;
  passwordHash: string;
  admin: boolean;
}

const userSchema: Schema<IUser> = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: String,
  email: { type: String, required: true, unique: true },
  phoneNumber: { type: String, required: true, unique: true },
  username: { type: String, required: true, unique: true },
  passwordHash: String,
  admin: Boolean
});
userSchema.plugin(uniqueValidator);

// export default mongoose.model<UserEntry>('User', userSchema);
export default mongoose.model<IUser>('User', userSchema);
