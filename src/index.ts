import app from './App';

const PORT = 3003;
app.listen(PORT, () => {
  console.log("Server running at 3003");
});
