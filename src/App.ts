import express from "express";
import config from "./utils/config";
import loginRouter from "./routes/login";
import signupRouter from "./routes/signup";
import productRouter from "./routes/product";
import mongoose from "mongoose";
import cors from "cors";
import categoriesRouter from "./routes/categories";
// import UrlEntry from "./types";


/* 

TODO
* Create users
  * create admin
* Make adnin only one to delete and update products
 */
const app = express();

const mongoUrl: string = config.MONGODB_URI as string;
// const mongoUrl = config.MONGODB_URI;
// console.log(mongoUrl);
// const url = 'mongodb+srv://damigreen:MX4FgbXXhaGDUp77@cluster0.6pe6n.mongodb.net/kicks?retryWrites=true&w=majority'

mongoose.connect(mongoUrl, { useNewUrlParser: true })
  .then(() => { console.log("connected to mongoDB"); })
  .catch((error: string) => { console.log(`error connecting to the db ${error}`); });


app.use(cors());
app.use(express.json());
// app.use(bodyParser().json());
app.use("/api/login", loginRouter);
app.use("/api/users", signupRouter);
app.use("/api/products", productRouter);
app.use("/api/categories", categoriesRouter);

app.get('/ping', (_req, res) => {
  res.send({ping: "Pong ======================="});
});

export default app;
