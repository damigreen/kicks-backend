// import supertest from "supertest";

// import app from "../App";
const supertest =require("supertest")
const app =require("../App")

const api = supertest(app);

describe('get products', () => {
  test("return products in json", async () => {
    await api
      .get("api/products")
      .expect(200)
      .expect("Content-Type", /application\/json/);  
  });
});
