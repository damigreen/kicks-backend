import dotenv from  "dotenv";
dotenv.config();

let MONGODB_URI = process.env.MONGO_URI;
const PORT = process.env.PORT;

if (process.env.NODE_ENV === 'test') {
  MONGODB_URI = process.env.TEST_MONGO_URI;
}

export default {
  MONGODB_URI,
  PORT,
};
