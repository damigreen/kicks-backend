import mongoose, { Document } from "mongoose";

export interface UserEntry extends mongoose.Document {
  firstName: string;
  lastName: string;
  email: string;
  number: number;
  username: string;
  password: string;
};

export interface CustomRequest<T> extends Request {
  body: T
}


// export interface ProductEntry extends mongoose.Document {
//   name: string;
//   models: string;
//   price: number;
//   quantity: number;
//   colors: [string];
//   comments: [string];
//   sizes: [number];
// };
export interface ProductEntry {
  name: string;
  models: string;
  price: number;
  quantity: number;
  colors: [string];
  comments: [string];
  sizes: [number];
};

export interface ProductBody {
  name: string;
  models: string;
  price: number;
  quantity: number;
  brand: string,
  color: string;
  colors: [string];
  size: string;
  sizes: [number];
  comments: [string];
  gender: string;
  image: string;
  category: string;
}

export interface UserBody {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  username: string;
  passwordHash: string; 
  admin: boolean;
}

export interface Requests {
  token: string;
  body: {
    name: string;
    models: string;
    price: number;
    quantity: number;
    brand: string,
    color: string;
    colors: [string];
    size: string;
    sizes: [number];
    comments: [string];
  };
}

export interface DecodedToken {
  username: string;
  id: string;
  iat: string;
}
export interface CategoryBody extends Document {
  product: string;
  product_id: string;
  category_name: string;
  category_description: string;
}