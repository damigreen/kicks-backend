# kicks Backend

## Table of Contents

* [Personal Details](#Personal Details)
* [Description](#Description)
* [Technologies](#Technologies)
* [Installation](#Installation)


## Description

The backend API for the kicks website. It is built on nodejs using express for routing and creating the application endpoints. It makes use of typescript for typechecking andnemploys mongodb for database management.

## Technologies

* [**Typescript**](https://www.typescriptlang.org/) - A JavaScript library for building user interfaces
* [**Nodejs**](nodejs.org/) - A JavaScript runtime built on Chrome's V8 JavaScript engine.
* [**Axios**](https://github.com/axios/axios) - A promise based HTTP client for the browser and nodejs
* [**Mongo DB**](https://www.mongodb.com/) - A cross-platform document-oriented database program
* [**Jest**](https://jestjs.io/) - a delightful JavaScript Testing Framework with a focus on simplicity.

## Installation

```bash
$ git clone https://gitlab.com/damigreen/kicks-backend.git
$ cd kicks-backend && yarn
$ yarn run dev
```
