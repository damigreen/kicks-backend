const mongoose = require("mongoose");
import { UserEntry, ProductEntry, Body } from './src/types';

// const config = require("./src/utils/config");

// const url = config.MONGODB_URI;
const url = 'mongodb+srv://damigreen:MX4FgbXXhaGDUp77@cluster0.6pe6n.mongodb.net/kicks?retryWrites=true&w=majority'

mongoose.connect(url, { useNewUrlParser: true })
  .then(reslt => { console.log(`connected to ${url}`); })
  .catch(error => { console.log(`error connecting to the db ${error}`); });

const productSchema = new mongoose.Schema({
  name: String,
  model: String,
  price: Number,
  quantity: Number,
  colors: { type: [String] },
  comments: { type: [String] },
  sizes: { type: [Number] },
});

const Product = mongoose.model<ProductEntry>("Product", productSchema);

const product = new Product({
  name: "Nike",
  model: "Air Nike",
  price: 45000,
  quantity: 43,
  colors: ["Green", "Blue", "Yellow"],
  comments: ["Verry comfortable", "Affordable"],
  sizes: [43, 44, 45, 46] 
});

product.save().then(() => {
  console.log("Product saved");
  mongoose.connection.close();
}).catch(err => {
  console.log(err);
});
